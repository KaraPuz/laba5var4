﻿#include <iostream>
#include <fstream>
#include <windows.h>
#include <string>
#include <algorithm>
#include <cctype>

using namespace std;



int main()
{
	setlocale(LC_ALL, "Rus");
	SetConsoleCP(1251);

	std::ifstream in("text.txt");
	std::ofstream out("output.txt");

	std::string s;
	char a[10];// это проверочное слово по буквам 
	int N;//количество выводимых слов 
	in >> N;
	in >> a;// >> b;

	std::string mas[5000];//новый массив для записи подходящих слов
	int size = 0;

	//чтение из файла
	while (in >> s) {

		s.erase(std::remove_if(s.begin(), s.end(), ispunct), s.end());

		for (int i = 0; i < s.length(); i++) {
			if (s[i] >= 'A' && s[i] <= 'Z') {
				s[i] += 32;
			}
		}

		//слово содержит проверочные буквы
		bool proverochnoe_slovo = false;
		for (int i = 0; i < s.length(); i++)
			if (s[i] == a[0] or s[i] == a[1] or s[i] == a[2] or s[i] == a[3] or s[i] == a[4] or s[i] == a[5] or s[i] == a[6] or s[i] == a[7] or s[i] == a[8] or s[i] == a[9])
			{
				proverochnoe_slovo = true;
				break;
			}

		//запись в массив подходящих по условию слов 
		if (!proverochnoe_slovo)
		{
			bool slovo = false;
			for (int i = 0; i < size; i++)
				if (s == mas[i])
				{
					slovo = true;
					break;
				}

			if (!slovo)
			{
				mas[size] = s;
				size++;
			}
		}
	}
	// Сортировка слов
	for (int i = 0; i < size - 1; i++) {
		for (int j = i + 1; j < size; j++) {
			if (mas[i].length() < mas[j].length()) {
				std::swap(mas[i], mas[j]);
			}
		}
	}
	// Вывод n первых слов
	for (int i = 0; i < N; i++) {
		out << mas[i] << endl;
	}






}
